#!/usr/bin/perl
#
# $Id: migrate_base.pl,v 1.8 2003/07/21 14:37:28 lukeh Exp $
#
# Copyright (c) 1997-2003 Luke Howard.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#        This product includes software developed by Luke Howard.
# 4. The name of the other may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE LUKE HOWARD ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL LUKE HOWARD BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
#
# LDIF entries for base DN
#
# 

require 'migrate_common.ph';

$PROGRAM = "migrate_sam_base.pl";

sub nbnsblock
{
	local ($field) = @_;
	print $HANDLE "dn: CN=$NISDOMAIN,CN=$field,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30DomainInfo\n";
	print $HANDLE "msSFU30MasterServerName: $NETBIOS_NAME\n";
	print $HANDLE "msSFU30OrderNumber: 10000\n";
	print $HANDLE "msSFU30Domains: $NISDOMAIN\n\n";
}

sub enable_nis_ypserv30
{
	local ($HANDLE) = @_;

	print $HANDLE "dn: CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n";
	print $HANDLE "schemaVersion: 40\n\n";

	print $HANDLE "dn: CN=mail,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byaddr,CN=mail,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=msSFU30MailAliases)\n\n";

	print $HANDLE "dn: CN=aliases,CN=mail,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=msSFU30MailAliases)\n\n";

	&nbnsblock('mail');

	print $HANDLE "dn: CN=services,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byname,CN=services,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: CN\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: CN\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: CN\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=ipService)\n\n";

	&nbnsblock('services');

	print $HANDLE "dn: CN=bootparams,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bydefaults,CN=bootparams,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: bootParameter\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: bootParameter\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (&(objectCategory=Device)(bootParameter=*))\n\n";

	&nbnsblock('bootparams');

	print $HANDLE "dn: CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bydefaults,CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: NisNetgroupTriple\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: NisNetgroupTriple\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=NisNetgroup)\n\n";

	print $HANDLE "dn: CN=byuser,CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30NetgroupUserAtDomain\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30SearchAttributes: MemberNisNetgroup\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30NetgroupUserAtDomain\n";
	print $HANDLE "msSFU30ResultAttributes: MemberNisNetgroup\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=NisNetgroup)\n\n";

	print $HANDLE "dn: CN=byhost,CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30NetgroupHostAtDomain\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30SearchAttributes: MemberNisNetgroup\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30NetgroupHostAtDomain\n";
	print $HANDLE "msSFU30ResultAttributes: MemberNisNetgroup\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=NisNetgroup)\n\n";

	&nbnsblock('netgroup');

	print $HANDLE "dn: CN=netmasks,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byaddr,CN=netmasks,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: ipNetmaskNumber\n";
	print $HANDLE "msSFU30SearchAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: ipNetmaskNumber\n";
	print $HANDLE "msSFU30ResultAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30MapFilter: (&(objectCategory=ipNetwork)(ipNetmaskNumber=*))\n\n";

	&nbnsblock('netmasks');

	print $HANDLE "dn: CN=hosts,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byaddr,CN=hosts,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: ipHostNumber\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30SearchAttributes: ipHostNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: ipHostNumber\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=Computer)\n\n";

	print $HANDLE "dn: CN=byname,CN=hosts,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30SearchAttributes: ipHostNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: ipHostNumber\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=Computer)\n\n";

	&nbnsblock('hosts');

	print $HANDLE "dn: CN=ethers,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byaddr,CN=ethers,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: macAddress\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30SearchAttributes: macAddress\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: macAddress\n";
	print $HANDLE "msSFU30MapFilter: (&(objectCategory=Device)(macAddress=*))\n\n";

	print $HANDLE "dn: CN=byname,CN=ethers,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30SearchAttributes: macAddress\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: macAddress\n";
	print $HANDLE "msSFU30MapFilter: (&(objectCategory=Device)(macAddress=*))\n\n";

	&nbnsblock('ethers');

	print $HANDLE "dn: CN=networks,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byaddr,CN=networks,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=ipNetwork)\n\n";

	print $HANDLE "dn: CN=byname,CN=networks,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: ipNetworkNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=ipNetwork)\n\n";

	&nbnsblock('networks');

	print $HANDLE "dn: CN=protocols,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bynumber,CN=protocols,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: ipProtocolNumber\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: ipProtocolNumber\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: ipProtocolNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=ipProtocol)\n\n";

	print $HANDLE "dn: CN=byname,CN=protocols,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30IntraFieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: ipProtocolNumber\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: ipProtocolNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=ipProtocol)\n\n";

	&nbnsblock('protocols');

	print $HANDLE "dn: CN=rpc,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bynumber,CN=rpc,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: oncRpcNumber\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30SearchAttributes: oncRpcNumber\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Aliases\n";
	print $HANDLE "msSFU30ResultAttributes: oncRpcNumber\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=oncRpc)\n\n";

	&nbnsblock('rpc');

	print $HANDLE "dn: CN=netid,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byname,CN=netid,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator: \"        \"\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30KeyValues\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30KeyValues\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=msSFU30NetId)\n\n";

	&nbnsblock('netid');

	print $HANDLE "dn: CN=passwd,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=byuid,CN=passwd,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: uidNumber\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30SearchAttributes: loginShell\n";
	print $HANDLE "msSFU30SearchAttributes: unixHomeDirectory\n";
	print $HANDLE "msSFU30SearchAttributes: gecos\n";
	print $HANDLE "msSFU30SearchAttributes: gidNumber\n";
	print $HANDLE "msSFU30SearchAttributes: uidNumber\n";
	print $HANDLE "msSFU30SearchAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: loginShell\n";
	print $HANDLE "msSFU30ResultAttributes: unixHomeDirectory\n";
	print $HANDLE "msSFU30ResultAttributes: gecos\n";
	print $HANDLE "msSFU30ResultAttributes: gidNumber\n";
	print $HANDLE "msSFU30ResultAttributes: uidNumber\n";
	print $HANDLE "msSFU30ResultAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (uidNumber=*)\n\n";

	print $HANDLE "dn: CN=byname,CN=passwd,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30SearchAttributes: loginShell\n";
	print $HANDLE "msSFU30SearchAttributes: unixHomeDirectory\n";
	print $HANDLE "msSFU30SearchAttributes: gecos\n";
	print $HANDLE "msSFU30SearchAttributes: gidNumber\n";
	print $HANDLE "msSFU30SearchAttributes: uidNumber\n";
	print $HANDLE "msSFU30SearchAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: loginShell\n";
	print $HANDLE "msSFU30ResultAttributes: unixHomeDirectory\n";
	print $HANDLE "msSFU30ResultAttributes: gecos\n";
	print $HANDLE "msSFU30ResultAttributes: gidNumber\n";
	print $HANDLE "msSFU30ResultAttributes: uidNumber\n";
	print $HANDLE "msSFU30ResultAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (uidNumber=*)\n\n";

	&nbnsblock('passwd');

	print $HANDLE "dn: CN=shadow,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bydefaults,CN=shadow,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30IntraFieldSeparator:: Og==\n";
	print $HANDLE "msSFU30SearchAttributes: shadowFlag\n";
	print $HANDLE "msSFU30SearchAttributes: shadowExpire\n";
	print $HANDLE "msSFU30SearchAttributes: shadowInactive\n";
	print $HANDLE "msSFU30SearchAttributes: shadowWarning\n";
	print $HANDLE "msSFU30SearchAttributes: shadowMax\n";
	print $HANDLE "msSFU30SearchAttributes: shadowMin\n";
	print $HANDLE "msSFU30SearchAttributes: shadowLastChange\n";
	print $HANDLE "msSFU30SearchAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: shadowFlag\n";
	print $HANDLE "msSFU30ResultAttributes: shadowExpire\n";
	print $HANDLE "msSFU30ResultAttributes: shadowInactive\n";
	print $HANDLE "msSFU30ResultAttributes: shadowWarning\n";
	print $HANDLE "msSFU30ResultAttributes: shadowMax\n";
	print $HANDLE "msSFU30ResultAttributes: shadowMin\n";
	print $HANDLE "msSFU30ResultAttributes: shadowLastChange\n";
	print $HANDLE "msSFU30ResultAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (uidNumber=*)\n\n";

	&nbnsblock('shadow');

	print $HANDLE "dn: CN=group,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bygid,CN=group,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: gidNumber\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: memberUid\n";
	print $HANDLE "msSFU30SearchAttributes: gidNumber\n";
	print $HANDLE "msSFU30SearchAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: memberUid\n";
	print $HANDLE "msSFU30ResultAttributes: gidNumber\n";
	print $HANDLE "msSFU30ResultAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (&(objectCategory=group)(gidNumber=*))\n\n";

	print $HANDLE "dn: CN=byname,CN=group,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator:: Og==\n";
	print $HANDLE "msSFU30IntraFieldSeparator: ,\n";
	print $HANDLE "msSFU30SearchAttributes: memberUid\n";
	print $HANDLE "msSFU30SearchAttributes: gidNumber\n";
	print $HANDLE "msSFU30SearchAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: memberUid\n";
	print $HANDLE "msSFU30ResultAttributes: gidNumber\n";
	print $HANDLE "msSFU30ResultAttributes: unixUserPassword\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (&(objectCategory=group)(gidNumber=*))\n\n";

	&nbnsblock('group');

	print $HANDLE "dn: CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: container\n\n";

	print $HANDLE "dn: CN=bydefaults,CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,$DEFAULT_BASE\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: msSFU30NISMapConfig\n";
	print $HANDLE "msSFU30KeyAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30FieldSeparator:: IA==\n";
	print $HANDLE "msSFU30IntraFieldSeparator:: IA==\n";
	print $HANDLE "msSFU30SearchAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30ResultAttributes: msSFU30Name\n";
	print $HANDLE "msSFU30MapFilter: (objectCategory=msSFU30YpServers)\n\n";

	&nbnsblock('ypservers');
}

sub gen_attr
{
	local ($HANDLE,$cn,$schema_type,$attribute_id,$schema_guid,$description,$name,$ldapdname) = @_;

	if (! defined($name)) {
		$name = $cn
	}

	if (! defined($ldapdname)) {
		$ldapdname = $cn
	}

	print $HANDLE "dn: CN=$cn,CN=Schema,CN=Configuration,$DEFAULT_BASE\n";
	print $HANDLE "cn: $cn\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: $schema_type\n";
	if ($schema_type eq 'attributeSchema') {
		print $HANDLE "attributeID: $attribute_id\n";
	} elsif ($schema_type eq 'classSchema') {
		print $HANDLE "governsID: $attribute_id\n";
	}
	print $HANDLE "schemaIdGuid:: $schema_guid\n";
	print $HANDLE "name: $name\n";
	print $HANDLE "lDAPDisplayName: $ldapdname\n";
	print $HANDLE "adminDisplayName: $ldapdname\n";
	print $HANDLE "adminDescription: $description\n";
}

sub attr_schema
{
	local ($HANDLE,$cn,$attribute_id,$schema_guid,$description,$attribute_syntax,$omsyntax,$singlevalued,$name,$ldapdname) = @_;

	&gen_attr($HANDLE,$cn,'attributeSchema',$attribute_id,$schema_guid,$description,$name,$ldapdname);
	print $HANDLE "attributeSyntax: $attribute_syntax\n";
	print $HANDLE "oMSyntax: $omsyntax\n";
	print $HANDLE "isSingleValued: $singlevalued\n\n";
}

sub class_schema
{
	local ($HANDLE,$cn,$attribute_id,$schema_guid,$description,$objclasscategory,$mustcontain,$maycontain,$show_advanced,$system_only,$name,$ldapdname) = @_;

	&gen_attr($HANDLE,$cn,'classSchema',$attribute_id,$schema_guid,$description,$name,$ldapdname);

	@mustcontain = @{ $mustcontain };
	@maycontain = @{ $maycontain };

	print $HANDLE "possSuperiors: top\n";
	print $HANDLE "subClassOf: top\n";
	print $HANDLE "objectClassCategory: $objclasscategory\n";
	foreach $_ (@mustcontain) {
		print $HANDLE "mustContain: $_\n";
	}
	foreach $_ (@maycontain) {
		print $HANDLE "mayContain: $_\n";
	}
	print $HANDLE "showInAdvancedViewOnly: $show_advanced\n";
	print $HANDLE "systemOnly: $system_only\n";
	print $HANDLE "defaultObjectCategory: CN=$cn,CN=Schema,CN=Configuration,$DEFAULT_BASE\n\n"
}

sub enable_mailext_p1
{
	local ($HANDLE) = @_;

	&attr_schema($HANDLE,'mailLocalAddress','2.16.840.1.113730.3.1.13','ZBL4pV9F99AfMIcZi10/Ww==','RFC822 email address of this recipient','2.5.5.5','22','FALSE');

	&attr_schema($HANDLE,'mailHost','2.16.840.1.113730.3.1.18','r09vDDVrdCGODC1k/ZSPKA==','FQDN of the SMTP/MTA of this recipient','2.5.5.5','22','TRUE');

	&attr_schema($HANDLE,'mailRoutingAddress','2.16.840.1.113730.3.1.47','W7rKWK8I7R0dvCmsse/iPA==','RFC822 routing address of this recipient','2.5.5.5','22','TRUE');

	&attr_schema($HANDLE,'rfc822MailMember','1.3.6.1.4.1.42.2.27.2.1.15','aB7do9Dx3LkCSVgvixllpg==','rfc822 mail address of group member(s)','2.5.5.5','22','FALSE');
}

sub enable_mailext_p2
{
	local ($HANDLE) = @_;

	&class_schema($HANDLE,'inetLocalMailRecipient','2.16.840.1.113730.3.2.147','56t5OSQvx/dnPkoWTaMdkQ==','Internet local mail recipient','3','',['mailLocalAddress','mailHost','mailRoutingAddress'],'FALSE','FALSE');

	&class_schema($HANDLE,'nisMailAlias','1.3.6.1.4.1.42.2.27.1.2.5','gMnYtZqCPTLAMXe3RZus8A==','NIS mail alias','1',['cn'],['rfc822MailMember'],'FALSE','FALSE');
}

sub gen_base
{
	local ($HANDLE) = @_;

	&ldif_entry(STDOUT,$NAMINGCONTEXT{fstab}, $DEFAULT_BASE);
}

sub main
{
	if ($ARGV[0] eq 'ypserv30') {
		&enable_nis_ypserv30(STDOUT);
	} elsif ($ARGV[0] eq 'mailext_p1') {
		&enable_mailext_p1(STDOUT);
	} elsif ($ARGV[0] eq 'mailext_p2') {
		&enable_mailext_p2(STDOUT);
	} else {
		&gen_base(STDOUT);
	}
}

&main;
