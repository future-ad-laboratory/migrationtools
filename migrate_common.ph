#!/usr/bin/perl
#
# $Id: migrate_common.ph,v 1.22 2003/04/15 03:09:33 lukeh Exp $
#
# Copyright (c) 1997-2003 Luke Howard.
# Copyright (c) 2020 Tanya.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#        This product includes software developed by Luke Howard.
# 4. The name of the other may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE LUKE HOWARD ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL LUKE HOWARD BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

#
# Common defines for MigrationTools
#

# Naming contexts. Key is $PROGRAM with migrate_ and .pl 
# stripped off. 
use File::Which;

$NETINFOBRIDGE = (-x "/usr/sbin/mkslapdconf");

# Select domain type
if (defined($ENV{'DOMAIN_TYPE'})) {
	$DOMAIN_TYPE = $ENV{'DOMAIN_TYPE'};
} else {
	$DOMAIN_TYPE = 'smb_ad'
}

if ($DOMAIN_TYPE eq 'smb_ad') {
	$NAMINGCONTEXT{'aliases'}           = "CN=aliases,CN=mail,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'fstab'}             = "CN=mounts,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'passwd'}            = "OU=People";
	$NAMINGCONTEXT{'netgroup_byuser'}   = "CN=byuser,CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'netgroup_byhost'}   = "CN=byhost,CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'group'}             = "OU=Group";
	$NAMINGCONTEXT{'netgroup'}          = "CN=netgroup,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'hosts'}             = "CN=hosts,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'networks'}          = "CN=networks,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'protocols'}         = "CN=protocols,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'rpc'}               = "CN=rpc,CN=ypServ30,CN=RpcServices,CN=System";
	$NAMINGCONTEXT{'services'}          = "CN=services,CN=ypServ30,CN=RpcServices,CN=System";
} elsif ($NETINFOBRIDGE) {
	$NAMINGCONTEXT{'aliases'}           = "cn=aliases";
	$NAMINGCONTEXT{'fstab'}             = "cn=mounts";
	$NAMINGCONTEXT{'passwd'}            = "cn=users";
	$NAMINGCONTEXT{'netgroup_byuser'}   = "cn=netgroup.byuser";
	$NAMINGCONTEXT{'netgroup_byhost'}   = "cn=netgroup.byhost";
	$NAMINGCONTEXT{'group'}             = "cn=groups";
	$NAMINGCONTEXT{'netgroup'}          = "cn=netgroup";
	$NAMINGCONTEXT{'hosts'}             = "cn=machines";
	$NAMINGCONTEXT{'networks'}          = "cn=networks";
	$NAMINGCONTEXT{'protocols'}         = "cn=protocols";
	$NAMINGCONTEXT{'rpc'}               = "cn=rpcs";
	$NAMINGCONTEXT{'services'}          = "cn=services";
} else {
	$NAMINGCONTEXT{'aliases'}           = "ou=Aliases";
	$NAMINGCONTEXT{'fstab'}             = "ou=Mounts";
	$NAMINGCONTEXT{'passwd'}            = "ou=People";
	$NAMINGCONTEXT{'netgroup_byuser'}   = "nisMapName=netgroup.byuser";
	$NAMINGCONTEXT{'netgroup_byhost'}   = "nisMapName=netgroup.byhost";
	$NAMINGCONTEXT{'group'}             = "ou=Group";
	$NAMINGCONTEXT{'netgroup'}          = "ou=Netgroup";
	$NAMINGCONTEXT{'hosts'}             = "ou=Hosts";
	$NAMINGCONTEXT{'networks'}          = "ou=Networks";
	$NAMINGCONTEXT{'protocols'}         = "ou=Protocols";
	$NAMINGCONTEXT{'rpc'}               = "ou=Rpc";
	$NAMINGCONTEXT{'services'}          = "ou=Services";
}

# Default DNS domain
$DEFAULT_MAIL_DOMAIN = 'padl.com';

# Default base 
$DEFAULT_BASE = 'dc=padl,dc=com';

# Turn this on for inetLocalMailReceipient
# sendmail support; add the following to 
# sendmail.mc (thanks to Petr@Kristof.CZ):
##### CUT HERE #####
#define(`confLDAP_DEFAULT_SPEC',`-h "ldap.padl.com"')dnl
#LDAPROUTE_DOMAIN_FILE(`/etc/mail/ldapdomains')dnl
#FEATURE(ldap_routing)dnl
##### CUT HERE #####
# where /etc/mail/ldapdomains contains names of ldap_routed
# domains (similiar to MASQUERADE_DOMAIN_FILE).
#$DEFAULT_MAIL_HOST = "mail.padl.com";

$NETBIOS_NAME = 'PADL';

# turn this on to support more general object clases
# such as person.
$EXTENDED_SCHEMA = 0;

# Comment this out if your ldap server does not support UTF8 encoding
$USE_UTF8 = 1;

# Uncomment these to exclude system users and groups
#$IGNORE_UID_BELOW = 1000;
#$IGNORE_GID_BELOW = 100;

# And here's the opposite for completeness
#$IGNORE_UID_ABOVE = 9999;
#$IGNORE_GID_ABOVE = 9999;

# Try to get settings from /etc/ldap/ldap.conf
if (-e '/etc/ldap/ldap.conf') {
	open (FILE, '/etc/ldap/ldap.conf');
	while (<FILE>) {
		chomp;
		next if ( $_ =~ /^\s*$/ );
		next if ( $_ =~ /^#/ );
		if ($_ =~ /\s/) {
			($key, $value) = split;
		} else {
			($key, $value) = split("\t+");
		}
		if (length $key && length $value && $key eq 'BASE') {
			$LDAP_CONF{'BASE'} = $value;
		}
	}
}

#
# allow environment variables to override predefines
#
##

# Set domain base, mail domain and other vars
($DEFAULT_BASE,$DEFAULT_MAIL_DOMAIN,$NISDOMAIN,$NETBIOS_NAME,$SAMBA_DOMAIN_SID) = &get_domain_data();

if (defined($ENV{'LDAP_DEFAULT_MAIL_HOST'})) {
	$DEFAULT_MAIL_HOST = $ENV{'LDAP_DEFAULT_MAIL_HOST'};
}

if (defined($ENV{'LDAP_EXTENDED_SCHEMA'})) {
	$EXTENDED_SCHEMA = $ENV{'LDAP_EXTENDED_SCHEMA'};
}

if (defined($ENV{'UID_ABOVE'})) {
	$IGNORE_UID_BELOW = $ENV{'UID_ABOVE'};
}

if (defined($ENV{'UID_BELOW'})) {
	$IGNORE_UID_ABOVE = $ENV{'UID_BELOW'};
}

if (defined($ENV{'GID_ABOVE'})) {
	$IGNORE_GID_BELOW = $ENV{'GID_ABOVE'};
}

if (defined($ENV{'GID_BELOW'})) {
	$IGNORE_GID_ABOVE = $ENV{'GID_BELOW'};
}

# Default Kerberos realm
if ($EXTENDED_SCHEMA) {
	$DEFAULT_REALM = $DEFAULT_MAIL_DOMAIN;
	$DEFAULT_REALM =~ tr/a-z/A-Z/;
}

if (-x "/usr/sbin/revnetgroup") {
	$REVNETGROUP = "/usr/sbin/revnetgroup";
} elsif (-x "/usr/lib/yp/revnetgroup") {
	$REVNETGROUP = "/usr/lib/yp/revnetgroup";
}

$classmap{'o'} = 'organization';
$classmap{'dc'} = 'domain';
$classmap{'l'} = 'locality';
$classmap{'ou'} = 'organizationalUnit';
$classmap{'c'} = 'country';
$classmap{'nismapname'} = 'nisMap';
$classmap{'cn'} = 'container';

sub net_polish
{
	local ($lsid,$samba_domain_sid,$domain) = @_;

	chomp $lsid;
	if (! defined($domain)) {
		($domain) = $lsid =~ m/\b[A-Za-z ]+ ([^,]*) [A-Za-z]+:/;
	}

	if (! defined($samba_domain_sid)) {
		($samba_domain_sid) = $lsid =~ m/\b[A-Za-z ]+: ([^,]*)/;
	}
	return($domain,$samba_domain_sid);
}

sub get_ad_info
{
	local ($nis_domain,$default_mail_domain,$samba_domain_sid) = @_;
	local ($output);

	$nis_domain =~ tr/a-z/A-Z/;
	$output = `wbinfo -D $nis_domain 2>/dev/null`;

	if ($? eq 0) {
		if (! defined($default_mail_domain)) {
			($default_mail_domain) = $output =~ m/\b.*\nAlt_Name[ ]+: ([a-z.]+)/;
		}
		if (! defined($samba_domain_sid)) {
			($samba_domain_sid) = $output =~ m/\b.*\nSID[ ]+: (S-[0-9\-]+)/;
		}
	}
	return($default_mail_domain,$samba_domain_sid);
}

sub get_domain_data
{
	local ($default_base,$default_mail_domain,$nis_domain,$netbios_name,$samba_domain_sid,$lsid);

	if (defined($ENV{'SAMBA_DOMAIN_SID'})) {
		$samba_domain_sid = $ENV{'SAMBA_DOMAIN_SID'};
	}

	if (defined($ENV{'NIS_DOMAIN'})) {
		$nis_domain = $ENV{'NIS_DOMAIN'};
		$nis_domain =~ tr/A-Z/a-z/;
	}

	if (defined($ENV{'NETBIOS_NAME'})) {
		$netbios_name = $ENV{'NETBIOS_NAME'};
	}

	if ((! defined($samba_domain_sid) || ! defined($nis_domain)) && which 'net') {
		$lsid = `net getdomainsid 2>/dev/null`;
		if ($? eq 0) {
			($nis_domain,$samba_domain_sid) = &net_polish($lsid,$nis_domain,$samba_domain_sid);
			$nis_domain =~ tr/A-Z/a-z/;
		}
	}

	if ((! defined($samba_domain_sid) || ! defined($netbios_name)) && which 'net') {
		$lsid = `net getlocalsid 2>/dev/null`;
		if ($? eq 0) {
			($netbios_name,$samba_domain_sid) = &net_polish($lsid,$netbios_name,$samba_domain_sid);
			$netbios_name =~ tr/A-Z/a-z/;
		}
	}

	if (! defined($netbios_name) && defined($ENV{'HOSTNAME'})) {
		$netbios_name = $ENV{'HOSTNAME'};
		$netbios_name =~ tr/A-Z/a-z/;
	}

	if (defined($ENV{'LDAP_BASEDN'})) {
		$default_base = $ENV{'LDAP_BASEDN'};
		$default_mail_domain = &basedn_expand($default_base);
	} elsif (defined($LDAP_CONF{'BASE'})) {
		$default_base = $LDAP_CONF{'BASE'};
		$default_mail_domain = &basedn_expand($default_base);
	}

	if (defined($ENV{'LDAP_DEFAULT_MAIL_DOMAIN'})) {
		$default_mail_domain = $ENV{'LDAP_DEFAULT_MAIL_DOMAIN'};
		if (! defined($default_base)) {
			$default_base = &domain_expand($default_mail_domain);
		}
	}

	if (! defined($nis_domain) && defined($default_base)) {
		($nis_domain) = $default_base =~ m/\b[A-Za-z]+=([^,]*)/;
		$nis_domain =~ tr/A-Z/a-z/;
	}

	if ((! defined($default_mail_domain) || ! defined($samba_domain_sid) && defined($nis_domain)) && which 'wbinfo') {
		($default_mail_domain,$samba_domain_sid) = &get_ad_info($nis_domain,$default_mail_domain,$samba_domain_sid);
		if (defined($default_mail_domain) && defined($samba_domain_sid)) {
			$default_base = &domain_expand($default_mail_domain);
		}
	}

	if ($DOMAIN_TYPE eq 'smb_ad' && defined($default_base)) {
		$default_base = &basedn_uppercase($default_base);
	}

	return($default_base,$default_mail_domain,$nis_domain,$netbios_name,$samba_domain_sid);
}

sub parse_args
{
	if ($#ARGV < 0) {
		print STDERR "Usage: $PROGRAM infile [outfile]\n";
		exit 1;
	}
	
	$INFILE = $ARGV[0];
	
	if ($#ARGV > 0) {
		$OUTFILE = $ARGV[1];
	}
}

sub open_files
{
	open(INFILE);
	if ($OUTFILE) {
		open(OUTFILE,">$OUTFILE");
		$use_stdout = 0;
	} else {
		$use_stdout = 1;
	}
}

# moved from migrate_hosts.pl
# lukeh 10/30/97
sub domain_expand
{
	local($first) = 1;
	local($dn);
	local(@namecomponents) = split(/\./, $_[0]);
	foreach $_ (@namecomponents) {
		if (! defined($done)) {
			$dn .= "dc=$_";
			$done = 1;
		} else {
			$dn .= ",dc=$_";
		}
	}
	return $dn;
}

# Expand basedn
sub basedn_expand
{
	local($first) = 1;
	local($dn);
	local(@namecomponents) = split(/,dc=/, $_[0]);
	foreach $_ (@namecomponents) {
		$first = 0;
		if ($_ =~ /^[a-zA-Z]+=/) {
			$dn .= $_ =~ s/^[a-zA-Z]+=//r
		} else {
			$dn .= ".$_";
		}
	}
	return $dn;
}

# Set uppercase basedn
sub basedn_uppercase
{
	local($first) = 1;
	local($dn);
	local(@namecomponents) = split(/,dc=/, $_[0]);
	foreach $_ (@namecomponents) {
		$first = 0;
		if ($_ =~ /^[a-zA-Z]+=/) {
			$dn .= $_ =~ s/^[a-zA-Z]+=/DC=/r
		} else {
			$dn .= ",DC=$_";
		}
	}
	return $dn;
}

# case insensitive unique
sub uniq
{
	local($name) = shift(@_);
	local(@vec) = sort {uc($a) cmp uc($b)} @_;
	local(@ret);
	local($next, $last);
	foreach $next (@vec) {
		if ((uc($next) ne uc($last)) &&
			(uc($next) ne uc($name))) {
			push (@ret, $next);
		}
		$last = $next;
	}
	return @ret;
}

# concatenate naming context and 
# organizational base
sub getsuffix
{
	local($program) = shift(@_);
	local($nc);
	$program =~ s/^migrate_(.*)\.pl$/$1/;
	$nc = $NAMINGCONTEXT{$program};
	if ($nc eq "") {
		return $DEFAULT_BASE;
	} else {
		return $nc . ',' . $DEFAULT_BASE;
	}
}

sub ldif_entry
{
# remove leading, trailing whitespace
	local ($HANDLE, $lhs, $rhs) = @_;
	local ($type, $val) = split(/\=/, $lhs);
	local ($dn);

	if ($rhs ne "") {
		$dn = $lhs . ',' . $rhs;
	} else {
		$dn = $lhs;
	}

	$type =~ s/\s*$//o;
	$type =~ s/^\s*//o;
	$type =~ tr/A-Z/a-z/;
	$val =~ s/\s*$//o;
	$val =~ s/^\s*//o;
	$val =~ s/,.*//;

	print $HANDLE "dn: $dn\n";
	print $HANDLE "$type: $val\n";
	print $HANDLE "objectClass: top\n";
	print $HANDLE "objectClass: $classmap{$type}\n";
	if ($EXTENDED_SCHEMA) {
		if ($DEFAULT_MAIL_DOMAIN) {
			print $HANDLE "objectClass: domainRelatedObject\n";
			print $HANDLE "associatedDomain: $DEFAULT_MAIL_DOMAIN\n";
		}
	}

	print $HANDLE "\n";
}

# Added Thu Jun 20 16:40:28 CDT 2002 by Bob Apthorpe
# <apthorpe@cynistar.net> to solve problems with embedded plusses in
# protocols and mail aliases.
sub escape_metacharacters
{
	local($name) = @_;

	# From Table 3.1 "Characters Requiring Quoting When Contained
	# in Distinguished Names", p87 "Understanding and Deploying LDAP
	# Directory Services", Howes, Smith, & Good.

	# 1) Quote backslash
	# Note: none of these are very elegant or robust and may cause
	# more trouble than they're worth. That's why they're disabled.
	# 1.a) naive (escape all backslashes)
	# $name =~ s#\\#\\\\#og;
	#
	# 1.b) mostly naive (escape all backslashes not followed by
	# a backslash)
	# $name =~ s#\\(?!\\)#\\\\#og;
	#
	# 1.c) less naive and utterly gruesome (replace solitary
	# backslashes)
	# $name =~ s{		# Replace
	#		(?<!\\) # negative lookbehind (no preceding backslash)
	#		\\	# a single backslash
	#		(?!\\)	# negative lookahead (no following backslash)
	#	}
	#	{		# With
	#		\\\\	# a pair of backslashes
	#	}gx;
	# Ugh. Note that s#(?:[^\\])\\(?:[^\\])#////#g fails if $name
	# starts or ends with a backslash. This expression won't work
	# under perl4 because the /x flag and negative lookahead and
	# lookbehind operations aren't supported. Sorry. Also note that
	# s#(?:[^\\]*)\\(?:[^\\]*)#////#g won't work either.  Of course,
	# this is all broken if $name is already escaped before we get
	# to it. Best to throw a warning and make the user import these
	# records by hand.

	# 2) Quote leading and trailing spaces
	local($leader, $body, $trailer) = ();
	if (($leader, $body, $trailer) = ($name =~ m#^( *)(.*\S)( *)$#o)) {
		$leader =~ s# #\\ #og;
		$trailer =~ s# #\\ #og;
		$name = $leader . $body . $trailer;
	}

	# 3) Quote leading octothorpe (#)
	$name =~ s/^#/\\#/o;

	# 4) Quote comma, plus, double-quote, less-than, greater-than,
	# and semicolon
	$name =~ s#([,+"<>;])#\\$1#g;

	return $name;
}

1;

