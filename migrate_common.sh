#!/bin/sh
#
# $Id: migrate_common.sh,v 1.0 2020/07/01 11:40:12 lukeh Exp $
#
# Copyright (c) 1997-2003 Luke Howard.
# Copyright (c) 2020 Tanya.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#        This product includes software developed by Luke Howard.
# 4. The name of the other may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE LUKE HOWARD ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL LUKE HOWARD BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
#
# Migrate all entities from flat files. 
#
# Make sure that you configure migrate_common.ph to suit
# your site's X.500 naming context and DNS mail domain;
# the defaults may not be correct.
#
# Luke Howard <lukeh@padl.com> April 1997
# Tanya <tanyadegurechaff@waifu.club> July 2020
#

# Defaults
[ -f "/etc/os-release" ] && . /etc/os-release
dirname=${dirname:-$(dirname "$0")}
MIGRATION_TYPE=${MIGRATION_TYPE:-full}

perlset () {
	# saves having to change #! path in each script
	if [ "X$PERL" = 'X' ]; then
		if [ -x /usr/local/bin/perl ]; then
			PERL="/usr/local/bin/perl"
		elif [ -x /usr/bin/perl ]; then
			PERL="/usr/bin/perl"
		else
			echo "Can't find Perl!"
			exit 1
		fi
	fi
}

perlexec () {
	spath=$1
	shift
	if [ -f "$dirname/$spath" ]; then
		$PERL "$dirname/$spath" $@
	elif [ -f "$spath" ]; then
		$PERL "$spath" $@
	else
		ppath=$(which "$spath")
		[ "x$ppath" != 'x' ] && $PERL "$ppath" $@
	fi
	return $?
}

of_getopts () {
	# Usage
	usage="Usage: $(basename "$_") [options]"
	usage=$usage'\n  -e\tExpand schema only (SMB)'
	usage=$usage'\n  -s\tExclude suffix (NT4)'
	usage=$usage'\n\tInclude suffix (SMB)'
	usage=$usage'\n  -m\tMinimal import'
	while getopts ':hesm' arg; do
		case $arg in
			h)
			echo "$usage"
			exit 0
			;;
			e)
			expand_schema_only=1
			;;
			s)
			toggle_suffix=1
			;;
			m)
			MIGRATION_TYPE='min'
			;;
			*)
			echo 'Not recognized option' >&2
			echo "$usage"
			exit 1
		esac
	done
}

on_getopts () {
	# Usage
	usage="Usage: $(basename "$_") [options]"
	usage=$usage'\n  -s\tInclude suffix'
	usage=$usage'\n  -a\tAuthentication'
	usage=$usage'\n  -m\tMinimal import'
	while getopts ':hsa:m' arg; do
		case $arg in
			h)
			echo "$usage"
			exit 0
			;;
			s)
			toggle_suffix=1
			;;
			a)
			SASL_MECH=${OPTARG}
			;;
			m)
			MIGRATION_TYPE='min'
			;;
			*)
			echo 'Not recognized option' >&2
			echo "$usage"
			exit 1
		esac
	done
}

readldapconf () {
	if [ -f "$1" ]; then
		while IFS= read -r line; do
			if printf %s "$line" | grep -E -q '^[A-Z\_]+[ 	]+.*$' 2>/dev/null; then 
				llkey=$(echo "$line" | awk '{print $1}')
				case $llkey in
					BASE)
					[ -z "$LDAP_BASEDN" ] && LDAP_BASEDN=$(echo "$line" | awk '{print $2}')
					;;
					URI)
					[ -z "$LDAP_URI" ] && LDAP_URI=$(echo "$line" | awk '{print $2}')
					;;
					SASL_MECH)
					[ -z "$SASL_MECH" ] && SASL_MECH=$(echo "$line" | awk '{print $2}')
					;;
				esac
			fi
		done < "$1"
	fi
}

dbgen () {
	DB="$(mktemp -t nis.ldif.XXXXXXXXXX)" || {
		echo "Can't create temporary file" >&2
		exit 1
	}
}

setvars_min () {
	if [ "X$ETC_GROUP" = 'X' ]; then
		ETC_GROUP=/etc/group
	fi
	if [ "X$ETC_PASSWD" = 'X' ]; then
		ETC_PASSWD=/etc/passwd
	fi
}

setvars_full () {
	if [ "X$ETC_ALIASES" = 'X' ]; then
		ETC_ALIASES=/etc/aliases
	fi
	if [ "X$ETC_FSTAB" = 'X' ]; then
		ETC_FSTAB=/etc/fstab
	fi
	if [ "X$ETC_HOSTS" = 'X' ]; then
		ETC_HOSTS=/etc/hosts
	fi
	if [ "X$ETC_NETWORKS" = 'X' ]; then
		ETC_NETWORKS=/etc/networks
	fi
	setvars_min
	if [ "X$ETC_SERVICES" = 'X' ]; then
		ETC_SERVICES=/etc/services
	fi
	if [ "X$ETC_PROTOCOLS" = 'X' ]; then
		ETC_PROTOCOLS=/etc/protocols
	fi
	if [ "X$ETC_RPC" = 'X' ]; then
		ETC_RPC=/etc/rpc
	fi
	if [ "X$ETC_NETGROUP" = 'X' ]; then
		ETC_NETGROUP=/etc/netgroup
	fi
}

setex_var () {
	[ "$1" = 'SLDAPADD or LDIF2LDBM' ] && echo "Can't find ldif2ldbm or slapadd!"
	echo "Please set the $1 environment variable to point to $2."
	echo
	exit 1
}

of_setexecs () {
	if [ "X$LDIF2LDBM" = 'X' ]; then
		if [ -x /usr/local/etc/ldif2ldbm ]; then
			LDIF2LDBM="/usr/local/etc/ldif2ldbm"
		elif [ -x /usr/local/sbin/ldif2ldbm ]; then
			LDIF2LDBM="/usr/local/sbin/ldif2ldbm"
		elif [ -x /usr/sbin/ldif2ldbm ]; then
			LDIF2LDBM="/usr/sbin/ldif2ldbm"
		elif [ -x "$NSHOME/bin/slapd/server/ns-slapd" ]; then
			LDIF2LDBM="$NSHOME/bin/slapd/server/ns-slapd ldif2db -f $NSHOME/slapd-$serverID"
		elif [ -x /usr/iplanet/servers/bin/slapd/server/dsimport ]; then
			LDIF2LDBM="/usr/iplanet/servers/bin/slapd/server/dsimport"
		elif which ldif2ldbm >/dev/null; then
			LDIF2LDBM=$(which ldif2ldbm)
		fi
	fi

	if [ "X$SLAPADD" = 'X' ]; then
		if [ -x /usr/local/sbin/slapadd ]; then
			SLAPADD="/usr/local/sbin/slapadd"
		elif [ -x /usr/sbin/slapadd ]; then
			SLAPADD="/usr/sbin/slapadd"
		elif which slapadd >/dev/null; then
			SLAPADD=$(which slapadd)
		fi
	fi

	if [ "X$LDBADD" = 'X' ]; then
		if [ -x ldbadd ]; then
			LDBADD="ldbadd"
		elif [ -x /usr/local/bin/ldbadd ]; then
			LDBADD="/usr/local/bin/ldbadd"
		elif [ -x /usr/bin/ldbadd ]; then
			LDBADD="/usr/bin/ldbadd"
		elif which ldbadd >/dev/null; then
			LDBADD=$(which ldbadd)
		fi
	fi

	if [ "X$LDBADD" = 'X' ] && ( [ "X$LDIF2LDBM" != 'X' ] || [ "X$SLAPADD" != 'X' ] ); then
		DOMAIN_TYPE=${DOMAIN_TYPE:-nt4}
	else
		DOMAIN_TYPE=${DOMAIN_TYPE:-smb_ad}
	fi
	export DOMAIN_TYPE=$DOMAIN_TYPE

	if [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		[ "X$LDIF2LDBM" = 'X' ] && [ "X$SLAPADD" = 'X' ] && setex_var 'SLDAPADD or LDIF2LDBM' 'sldapadd or ldif2ldm'
	elif [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		[ "X$LDBADD" = 'X' ] && setex_var 'LDBADD' 'ldbadd'
	fi
}

on_setexecs () {
	if [ "X$LDAPADD" = 'X' ]; then
		if [ -x ldapadd ]; then
			LDAPADD="ldapadd -c"
		elif [ -x /usr/local/bin/ldapadd ]; then
			LDAPADD="/usr/local/bin/ldapadd -c"
		elif [ -x /usr/bin/ldapadd ]; then
			LDAPADD="/usr/bin/ldapadd -c"
		elif which ldapadd >/dev/null; then
			LDAPADD="$(which ldapadd) -c"
		elif [ -x "$NSHOME/bin/slapd/server/ldapmodify" ]; then
			LDAPADD="$NSHOME/bin/slapd/server/ldapmodify -a -c"
		elif [ -x /usr/iplanet/servers/shared/bin/ldapmodify ]; then
			LDAPADD="/usr/iplanet/servers/shared/bin/ldapmodify -a -c"
		elif which ldapmodify >/dev/null; then
			LDAPADD="$(which ldapmodify) -a -c"
		fi
	fi

	if [ "X$LDBADD" = 'X' ]; then
		if [ -x ldbadd ]; then
			LDBADD="ldbadd"
		elif [ -x /usr/local/bin/ldbadd ]; then
			LDBADD="/usr/local/bin/ldbadd"
		elif [ -x /usr/bin/ldbadd ]; then
			LDBADD="/usr/bin/ldbadd"
		elif which ldbadd >/dev/null; then
			LDBADD=$(which ldbadd)
		fi
	fi

	if [ "X$LDAPADD" != 'X' ] && [ "X$LDBADD" = 'X' ]; then
		DOMAIN_TYPE=${DOMAIN_TYPE:-nt4}
	else
		DOMAIN_TYPE=${DOMAIN_TYPE:-smb_ad}
	fi
	export DOMAIN_TYPE=$DOMAIN_TYPE

	if [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		[ "X$LDAPADD" = 'X' ] && setex_var 'LDAPADD' 'ldapadd'
	elif [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		[ "X$LDBADD" = 'X' ] && setex_var 'LDBADD' 'ldbadd'
	fi
}

on_questions () {
	if [ "X$LDAP_BASEDN" = 'X' ]; then
		question="Enter the X.500 naming context you wish to import into: [$defaultcontext]"
		echo "$question " | tr -d '\012' > /dev/tty
		read LDAP_BASEDN
		if [ "X$LDAP_BASEDN" = 'X' ]; then
			if [ "X$defaultcontext" = 'X' ]; then
				echo "You must specify a default context."
				exit 2
			else
				LDAP_BASEDN=$defaultcontext
			fi
		fi
	fi
	export LDAP_BASEDN

	if [ "X$LDAP_URI" = 'X' ]; then
		question="Enter the hostname or URI of your LDAP server [ldap://localhost]:"
		echo "$question " | tr -d '\012' > /dev/tty
		read LDAP_URI
	fi

	if [ "X$LDAP_URI" = 'X' ]; then
		LDAP_URI="ldap://localhost"
		ldap_uri=1
	elif printf %s "$LDAP_URI" | grep -E -q '^[a-z]+://.*$' 2>/dev/null; then 
		ldap_uri=1
	else
		ldap_uri=0
	fi

	if [ "X$SASL_MECH" != 'XEXTERNAL' ] && [ "X$SASL_MECH" != 'XNO_AUTH' ]; then
		if [ "X$LDAP_BINDDN" = 'X' ]; then
			if [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
				defdn="Administrator"
			else
				defdn="cn=manager,$LDAP_BASEDN"
				[ "x$ID_LIKE" = 'xdebian' ] && defdn="cn=admin,$LDAP_BASEDN"
			fi
			question="Enter the manager DN: [$defdn]:"
			echo "$question " | tr -d '\012' > /dev/tty
			read LDAP_BINDDN
			if [ "X$LDAP_BINDDN" = 'X' ]; then
				LDAP_BINDDN="$defdn"
			fi
		fi
		export LDAP_BINDDN

		if [ "X$LDAP_BINDCRED" = 'X' ]; then
			question="Enter the credentials to bind with:"
			echo "$question " | tr -d '\012' > /dev/tty
			stty -echo
			read LDAP_BINDCRED
			stty echo
			echo
		fi
	fi

	if [ "X$LDAP_PROFILE" = 'X' ]; then
		question="Do you wish to generate a DUAConfigProfile [yes|no]?"
		echo "$question " | tr -d '\012' > /dev/tty
		read LDAP_PROFILE
		if [ "X$LDAP_PROFILE" = 'X' ]; then
			LDAP_PROFILE="no"
		fi
	fi
}

common_main () {
	if { [ -z "$SAMBA_DOMAIN_SID" ] || [ -z "$NIS_DOMAIN" ]; } && command -v net >/dev/null; then
		lsid=$(net getdomainsid 2>/dev/null)
		[ "$?" = 0 ] && {
			export SAMBA_DOMAIN_SID=${SAMBA_DOMAIN_SID:-${lsid#*: }}
			if [ -z "$NIS_DOMAIN" ]; then
				NIS_DOMAIN=${lsid% is:*}
				NIS_DOMAIN=${NIS_DOMAIN##* }
				export NIS_DOMAIN=$(echo "$NIS_DOMAIN" | tr '[:upper:]' '[:lower:]')
			fi
		}
	fi

	if ( [ -z "$SAMBA_DOMAIN_SID" ] || [ -z "$NETBIOS_NAME" ] ) && command -v net >/dev/null; then
		lsid=$(net getdomainsid 2>/dev/null)
		[ "$?" = 0 ] && {
			export SAMBA_DOMAIN_SID=${SAMBA_DOMAIN_SID:-${lsid#*: }}
			if [ -z "$NETBIOS_NAME" ]; then
				NETBIOS_NAME=${NETBIOS_NAME% is:*}
				export NETBIOS_NAME=${NETBIOS_NAME##* }
			fi
		}
	fi

	if command -v hostname >/dev/null; then
		export NETBIOS_NAME=${NETBIOS_NAME:-$(hostname -s | tr '[:lower:]' '[:upper:]')}
	fi

	if [ "X$NETBIOS_NAME" != 'X' ] && { { [ "X$LDAP_BASEDN" = 'X' ] && [ "X$LDAP_DEFAULT_MAIL_DOMAIN" = 'X' ]; } || [ "X$SAMBA_DOMAIN_SID" = 'X' ]; }; then
		wboutput=$(wbinfo -D "$NETBIOS_NAME")
		[ "X$LDAP_BASEDN" = 'X' ] && [ "X$LDAP_DEFAULT_MAIL_DOMAIN" = 'X' ] && export LDAP_DEFAULT_MAIL_DOMAIN=$(echo "$wboutput" | grep 'Alt_Name' | awk '{print $3}')
		[ "X$SAMBA_DOMAIN_SID" = 'X' ] && export SAMBA_DOMAIN_SID=$(echo "$wboutput" | grep 'SID' | awk '{print $3}')
	fi

	# Set files positions
	[ "$MIGRATION_TYPE" = 'min' ] && setvars_min || setvars_full
}

all_mig_min () {
	echo "Migrating groups..."
	perlexec migrate_group.pl			$ETC_GROUP >> $DB
	echo "Migrating users..."
	perlexec migrate_passwd.pl			$ETC_PASSWD >> $DB
}

all_mig_full () {
	#if [ "X$LDAP_PROFILE" = "Xyes" ]; then
	#	echo "Creating DUAConfigProfile entry..."
	#	$PERL migrate_profile.pl "$LDAP_URI" >> $DB
	#fi
	# Don't migrate aliases if it's specified to not do so
	if [ "X$MIGRATE_ALIASES" != 'Xfalse' ]; then
		echo "Migrating aliases..."
		perlexec migrate_aliases.pl 	$ETC_ALIASES >> $DB
	fi
	# Migrate fstab only if specifically asked
	if [ "X$MIGRATE_FSTAB" = 'Xtrue' ]; then
		echo "Migrating fstab..."
		perlexec migrate_fstab.pl		$ETC_FSTAB >> $DB
	fi
	echo "Migrating hosts..."
	perlexec migrate_hosts.pl			$ETC_HOSTS >> $DB
	echo "Migrating networks..."
	perlexec migrate_networks.pl		$ETC_NETWORKS >> $DB
	#all_mig_min
	echo "Migrating protocols..."
	perlexec migrate_protocols.pl		$ETC_PROTOCOLS >> $DB
	echo "Migrating rpcs..."
	perlexec migrate_rpc.pl				$ETC_RPC >> $DB
	echo "Migrating services..."
	perlexec migrate_services.pl		$ETC_SERVICES >> $DB
	echo "Migrating netgroups..."
	perlexec migrate_netgroup.pl		$ETC_NETGROUP >> $DB
	echo "Migrating netgroups (by user)..."
	perlexec migrate_netgroup_byuser.pl	$ETC_NETGROUP >> $DB
	echo "Migrating netgroups (by host)..."
	perlexec migrate_netgroup_byhost.pl	$ETC_NETGROUP >> $DB
}

mig_update () {
	if [ "$1" = 1 ]; then
		on_update "$ldifile" || {
			echo "Import failed" >&2
			exit 1
		}
	else
		of_update "$ldifile" || {
			echo "Import failed" >&2
			exit 1
		}
	fi
}

conf_schema () {
	local ldifile="$(mktemp -t tmp.ldif.XXXXXXXXXX)" || {
		echo "Can't create temporary file" >&2
		exit 1
	}
	perlexec migrate_sam_base.pl mailext_p1 > $ldifile
	mig_update 0 "$ldifile"
	perlexec migrate_sam_base.pl mailext_p2 > $ldifile
	mig_update 0 "$ldifile"
}

migrate () {
	local online=$1
	local online=${online:-0}

	[ "$online" = 1 ] && {
		echo
		echo "Importing into $LDAP_BASEDN..."
		echo
	}

	echo "Creating naming context entries..."
	if [ "$online" = 1 ] && [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		INCLUDE_SUFFIX=${INCLUDE_SUFFIX:-1}
	else
		INCLUDE_SUFFIX=${INCLUDE_SUFFIX:-0}
	fi
	[ "$toggle_suffix" = 1 ] && INCLUDE_SUFFIX=$((! INCLUDE_SUFFIX))

	if [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		if [ "$INCLUDE_SUFFIX" = 0 ]; then
			perlexec migrate_ldap_base.pl > $1
		elif [ "$INCLUDE_SUFFIX" = 1 ]; then
			perlexec migrate_ldap_base.pl -n > $1
		fi
	fi

	if [ "X$MIGRATION_TYPE" = 'Xfull' ]; then
		if [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
			[ "$INCLUDE_SUFFIX" = 1 ] && {
			echo "Enabling RFC2307"
				perlexec migrate_sam_base.pl ypserv30 > $DB
			}
			perlexec migrate_sam_base.pl > $DB
		fi
		all_mig_full
	else
		all_mig_min
	fi
}

ifexit () {
	if [ "X$EXIT" != 'Xno' ]; then
		exit $EXITCODE
	fi
}

fail_control () {
	if [ "$3" -ne 0 ]; then
		[ "$1" = 1 ] && echo "$4: returned non-zero exit status: saving failed LDIF to $2" || echo "Migration failed: saving failed LDIF to $2"
	else
		[ "$1" = 1 ] && [ ! -z "$4" ] && echo "$4: succeeded"
		rm -f $2
	fi
}

of_update () {
	if [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		echo "Preparing LDB database..."
	elif [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		echo "Preparing LDAP database..."
	fi

	if [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		if [ "X$SLAPADD" = 'X' ]; then
			$LDIF2LDBM -i $1
		else
			$SLAPADD -l $1
		fi
	elif [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		if [ -f '/usr/local/samba/private/sam.ldb' ]; then
			SAMDB=${SAMDB:-'/usr/local/samba/private/sam.ldb'}
		elif [ -f '/var/lib/samba/private/sam.ldb' ]; then
			SAMDB=${SAMDB:-'/var/lib/samba/private/sam.ldb'}
		fi
		$LDBADD --option="dsdb:schema update allowed"=true -H "$SAMDB" "$1"
	fi
	EXITCODE=$?

	fail_control 1 "$1" "$EXITCODE"

	echo "Done."
	return $EXITCODE
}

on_update () {
	if [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		echo "Importing into LDB..."
	elif [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		echo "Importing into LDAP..."
	fi

	if [ "X$DOMAIN_TYPE" = 'Xnt4' ]; then
		case $LDAPADD in
			*'ldapadd'*)
			[ $ldap_uri = 1 ] && ldaphostcmd="-H $LDAP_URI" || ldaphostcmd="-h $LDAP_URI"
			ldapcmd="$LDAPADD $ldaphostcmd"
			if [ "x$SASL_MECH" != 'x' ]; then
				ldapendcmd="-Y $SASL_MECH -f $1"
			else
				ldapendcmd="-f $1"
			fi
			if [ "X$SASL_MECH" != 'XEXTERNAL' ] && [ "X$LDAP_BINDCRED" = 'X' ]; then
				$ldapcmd -D "$LDAP_BINDDN" $ldapendcmd
			elif [ "X$SASL_MECH" != 'XEXTERNAL' ]; then
				$ldapcmd -D "$LDAP_BINDDN" -w "$LDAP_BINDCRED" $ldapendcmd
			else
				$ldapcmd $ldapendcmd
			fi
			EXITCODE=$?
			;;
			*)
			false
			;;
		esac
	elif [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		if [ "X$LDAP_BINDCRED" = 'X' ]; then
			$LDBADD --option="dsdb:schema update allowed"=true -H "$LDAP_URI" -U "$LDAP_BINDDN" "$1"
		else
			$LDBADD --option="dsdb:schema update allowed"=true -H "$LDAP_URI" -U "$LDAP_BINDDN%$LDAP_BINDCRED" "$1"
		fi
		EXITCODE=$?
	fi
	fail_control 1 "$1" "$EXITCODE" "$LDBADD"
	return $EXITCODE
}

tmpfd_gen () {
	TEMP_DIR="$(mktemp -dt)" || {
		echo "Can't create temporary directory" >&2
		exit 1
	}
	ETC_PASSWD="$TEMP_DIR/passwd.ldap"
	ETC_GROUP="$TEMP_DIR/group.ldap"
	if [ "$MIGRATION_TYPE" != 'min' ]; then
		ETC_SERVICES="$TEMP_DIR/services.ldap"
		ETC_PROTOCOLS="$TEMP_DIR/protocols.ldap"
		[ ! -z "$(echo "$@" | grep 'fstab')" ] && ETC_FSTAB="$TEMP_DIR/fstab.ldap"
		ETC_RPC="$TEMP_DIR/rpc.ldap"
		ETC_HOSTS="$TEMP_DIR/hosts.ldap"
		ETC_NETWORKS="$TEMP_DIR/networks.ldap"
		[ ! -z "$(echo "$@" | grep 'netgroup')" ] && ETC_NETGROUP="$TEMP_DIR/netgroup.ldap"
		ETC_ALIASES="$TEMP_DIR/aliases.ldap"
	fi
	EXIT=no
}

tmpfd_destroy () {
	rm -f $ETC_PASSWD
	rm -f $ETC_GROUP
	if [ "$MIGRATION_TYPE" != 'min' ]; then
		rm -f $ETC_SERVICES
		rm -f $ETC_PROTOCOLS
		[ ! -z "$(echo "$@" | grep 'fstab')" ] && rm -f $ETC_FSTAB
		rm -f $ETC_RPC
		rm -f $ETC_HOSTS
		rm -f $ETC_NETWORKS
		[ ! -z "$(echo "$@" | grep 'netgroup')" ] && rm -f $ETC_NETGROUP
		rm -f $ETC_ALIASES
	fi
	rm -d $TEMP_DIR
}

of_main () {
	# Set executables positions
	of_setexecs
	# Common operations
	common_main
	# Generate LDIF temporary file
	dbgen
	# Expand SAMBA schema
	if [ "X$DOMAIN_TYPE" = 'Xsmb_ad' ]; then
		conf_schema
	fi
	if [ "X$DOMAIN_TYPE" != 'Xsmb_ad' ] || [ "$expand_schema_only" != 1 ]; then
		# Migrate data
		migrate
		# Bump data
		of_update "$DB"
	fi
	ifexit
}

on_main () {
	# Set executables positions
	on_setexecs
	# Common operations
	common_main
	# Ask questions for online push
	on_questions
	# Generate LDIF temporary file
	dbgen
	# Migrate data
	migrate 1
	# Bump data
	on_update "$DB"
	ifexit
}
