#!/bin/sh
#
# $Id: migrate_all_netinfo_offline.sh,v 1.4 2004/09/24 05:49:08 lukeh Exp $
#
# Copyright (c) 1997-2003 Luke Howard.
# Copyright (c) 2020 Tanya.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#        This product includes software developed by Luke Howard.
# 4. The name of the other may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE LUKE HOWARD ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL LUKE HOWARD BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

#
# Migrates NetInfo accounts using ldif2ldbm
#

# Defaults
PATH=$PATH:.
export PATH
dirname=$(dirname "$0")

# Import
if [ -f "$dirname/migrate_common.sh" ]; then
	. "$dirname/migrate_common.sh"
elif [ -f "/usr/share/migrationtools/migrate_common.sh" ]; then
	. "/usr/share/migrationtools/migrate_common.sh"
fi

# Set perl vars
perlset
# Get arguments
of_getopts $@
# Generate temp folder
tmpfd_gen fstab

question="Enter the NetInfo domain to import from [/]:"
echo "$question " | tr -d '\012' > /dev/tty
read DOM
if [ "X$DOM" = "X" ]; then
	DOM="/"
fi

nidump passwd $DOM > $ETC_PASSWD
nidump group $DOM > $ETC_GROUP
if [ "$MIGRATION_TYPE" != 'min' ]; then
	nidump services $DOM > $ETC_SERVICES
	nidump protocols $DOM > $ETC_PROTOCOLS
	nidump fstab $DOM > $ETC_FSTAB
	nidump rpc $DOM > $ETC_RPC
	nidump hosts $DOM > $ETC_HOSTS
	nidump networks $DOM > $ETC_NETWORKS
	nidump aliases $DOM > $ETC_ALIASES
fi

# Call main functions
of_main
# Destroy temp folder
tmpfd_destroy fstab
